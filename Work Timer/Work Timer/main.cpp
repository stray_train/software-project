// A small utility program done by Ahmed Elgoni, 528534
// The usage of ctime was learnt at http://stackoverflow.com/questions/997946/how-to-get-current-time-and-date-in-c, last accessed on 2014/03/31

#include <iostream>
#include <ctime>
#include <sstream>
#include <fstream>

using namespace std;

// Gets the current time in the format hh:mm:ss
string GetCurrentTime()
{
    string output;

    time_t rawtime;
    struct tm* timeinfo;

    time(&rawtime);
    timeinfo = localtime(&rawtime);
    string timeString = asctime(timeinfo);

    for (int i = 11; i < 19; i++)
        output = output + timeString[i];

    return output;
}

/// Returns the number of minutes passed since a specified time presented as a string in the form hh:mm:ss
int GetTimeDifference(string TimeString)
{
    string prevTime = TimeString;
    string curTime = GetCurrentTime();

    stringstream ss; // Used for conversion from string to int
    int hourDifference, minuteDifference; // Not counting seconds passed, too much information

    string tmpString1, tmpString2 = "";
    int tmpNum1, tmpNum2 = -1;

    // Start by getting the difference in hours
    for (int i = 0; i < 2; i++) tmpString1 = tmpString1 + curTime[i]; // the hh part
    for (int i = 0; i < 2; i++) tmpString2 = tmpString2 + prevTime[i]; // the hh part

    ss << tmpString1; ss >> tmpNum1;

    ss.str(""); ss.clear(); // Reset the string stream

    ss << tmpString2; ss >> tmpNum2;

    hourDifference = tmpNum1 - tmpNum2;

    // Next up get the number of minutes that elapsed
    tmpString1 = ""; tmpString2 = "";
    for (int i = 3; i < 5; i++) tmpString1 = tmpString1 + curTime[i]; // the mm part
    for (int i = 3; i < 5; i++) tmpString2 = tmpString2 + prevTime[i]; // the mm part

    ss.str(""); ss.clear(); // Reset the string stream
    ss << tmpString1; ss >> tmpNum1;
    ss.str(""); ss.clear(); // Reset the string stream
    ss << tmpString2; ss >> tmpNum2;

    minuteDifference = tmpNum1 - tmpNum2;

    return (hourDifference * 60) + minuteDifference;
}

int main()
{
    cout << "What are you working on?" << endl << endl;;

    int operationCode = -1;

    cout << "Research: 1" << endl;
    cout << "Design: 2" << endl;
    cout << "Coding: 3" << endl;
    cout << "Testing: 4" << endl;
    cout << "Documentation: 5" << endl << endl;

    cout << "I'm working on: ";
    cin >> operationCode;

    // Make sure we have the correct code
    while (operationCode < 1 || operationCode > 5)
    {
        cout << "operationCode is invalid input. Correct input is in the range [1-5]";
        cin >> operationCode;
    }

    switch (operationCode)
    {
        case 1: cout << "Research" << endl; break;
        case 2: cout << "Design" << endl; break;
        case 3: cout << "Coding" << endl; break;
        case 4: cout << "Testing" << endl; break;
        case 5: cout << "Documentation" << endl; break;
    }

    string startingTime = GetCurrentTime();
    string pauseString; // Used to stop the program from exiting

    cout << "The timer has been started, type in any input to end the current activity and log the time ";

    // [0]researchTime, [1]designTime, [2]codingTime, [3]testingTime, [4]documentationTime;
    string statusText[5]; // A dummy variable. The text file is formatted status_text timeInMuntes
    int times[5];
    ifstream infile("Timesheet.txt");

    for (int i = 0; i < 5; i++)
    {
        infile >> statusText[i] >> times[i];
    }

    infile.close();

    cin >> pauseString;

    // We're done, output the updated values

    times[operationCode - 1] += GetTimeDifference(startingTime);

    ofstream outFile;
    outFile.open("Timesheet.txt");

    for (int i = 0; i < 5; i++)
    {
        outFile << statusText[i] << " " << times[i] << endl;
    }

    outFile.close();

    return 0;
}
