// 528534
// Ahmed Elgoni

#ifndef DATAENTRY_H
#define DATAENTRY_H

#include <string>

class DataEntry
{
    public:
        DataEntry();
        void setYear(int Year);
        void setMonth(int Month);
        void setDay(int Day);
        void setHour(int Hour);
        void setMinute(int Minute);
        void setSecond(float Second);
        void setVoltage(float Voltage);
        void setCurrent(float Current);
        void setFrequency(float Frequency);
        void setValues(int Year, int Month, int Day, int Hour, int Minute,
                       float Second, float Voltage, float Current, float Frequency);

        int getYear();
        int getMonth();
        int getDay();
        int getHour();
        int getMinute();
        float getSecond();
        float getVoltage();
        float getCurrent();
        float getFrequency();
        float getPower();
        float getPowerRMS();
        bool isValid();

    protected:
    private:
        int _year, _month, _day, _hour, _minute;
        float _second, _voltage, _current, _frequency;
};

#endif // DATAENTRY_H
