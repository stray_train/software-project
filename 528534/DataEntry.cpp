// 528534
// Ahmed Elgoni

#include "DataEntry.h"
#include <cmath>
#include <iostream>

using namespace std;

DataEntry::DataEntry()
{
    //ctor
}

void DataEntry::setYear(int Year)
{
    if (Year > 0)
        _year = Year;
    else
    {
        _year = -1;
        //cout << "Incorrect Year: " << Year << endl;
    }

}

void DataEntry::setMonth(int Month)
{
    if (Month >= 1 && Month <= 12)
        _month = Month;
    else
    {
        _month = -1;
        //cout << "Incorrect Month: " << Month << endl;
    }

}

void DataEntry::setDay(int Day)
{
    if (Day > 1 && Day <= 31)
        _day = Day;
    else
    {
        _day = -1;
        //cout << "Incorrect Day: " << Day << endl;
    }

}

void DataEntry::setHour(int Hour)
{
    if (Hour >= 0 && Hour <= 23)
        _hour = Hour;
    else
    {
        _hour = -1;
        //cout << "Incorrect Hour: " << Hour << endl;
    }
}

void DataEntry::setMinute(int Minute)
{
    if (Minute >= 0 && Minute <= 59)
        _minute = Minute;
    else
    {
        _minute = -1;
        //cout << "Incorrect Minute: " << Minute << endl;
    }

}

void DataEntry::setSecond(float Second)
{
    if (Second >= 0 && Second < 60)
        _second = Second;
    else
    {
        _second = -1;
        //cout << "Incorrect Second: " << Second << endl;
    }

}

void DataEntry::setVoltage(float Voltage)
{
    _voltage = Voltage; // It's assumed that voltage can be positive or negative so no error checking is required
}

void DataEntry::setCurrent(float Current)
{
    _current = Current; // It's assumed that current can be positive or negative so no error checking is required
}

void DataEntry::setFrequency(float Frequency)
{
    if (Frequency >= 0)
        _frequency = Frequency;
    else
    {
        _frequency = -1;
        //cout << "Incorrect Frequency: " << Frequency << endl;
    }

}

void DataEntry::setValues(int Year, int Month, int Day, int Hour, int Minute, float Second, float Voltage, float Current, float Frequency)
{
    setYear(Year);
    setMonth(Month);
    setDay(Day);
    setHour(Hour);
    setMinute(Minute);
    setSecond(Second);
    setVoltage(Voltage);
    setCurrent(Current);
    setFrequency(Frequency);
}

int DataEntry::getYear()
{
    return _year;
}

int DataEntry::getMonth()
{
    return _month;
}

int DataEntry::getDay()
{
    return _day;
}

int DataEntry::getHour()
{
    return _hour;
}

int DataEntry::getMinute()
{
    return _minute;
}

float DataEntry::getSecond()
{
    return _second;
}

float DataEntry::getVoltage()
{
    return _voltage;
}

float DataEntry::getCurrent()
{
    return _current;
}

float DataEntry::getFrequency()
{
    return _frequency;
}

float DataEntry::getPower()
{
    return (_voltage * sqrt(2)) * (_current * sqrt(2));
}

float DataEntry::getPowerRMS()
{
    return _voltage * _current;
}

bool DataEntry::isValid()
{
    // The value -1 is an error flag set by the setX functions
    if (_year == -1 || _month == -1 || _day == -1 || _hour == -1 || _minute == -1 || _second == -1 || _frequency == -1)
        return false;
    else
        return true;
}
