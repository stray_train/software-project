// 528534
// Ahmed Elgoni

#include <fstream>
#include <cmath>
#include <vector>
#include "DataEntry.h"

using namespace std;

void ReadFromFile(float& TotalEnergy, float& MaxPower, float& MeanPower, vector<float>& EnergyPerHourVector);
void WriteToFiles(float TotalEnergy, float MaxPower, float MeanPower, const vector<float>& EnergyPerHourVector);
float GetTimeDifference(DataEntry CurrentEntry, DataEntry PreviousEntry); // Returns the time elapsed in seconds between two data entries


int main()
{
    float totalEnergy = 0;
    float maxPower = 0;
    float meanPower = 0;
    vector<float> energyPerHour(24, 0);

    ReadFromFile(totalEnergy, maxPower, meanPower, energyPerHour);
    WriteToFiles(totalEnergy, maxPower, meanPower, energyPerHour);

    return 0;
}

void ReadFromFile(float& TotalEnergy, float& MaxPower, float& MeanPower, vector<float>& EnergyPerHourVector)
{
    // Temporary variables used to process a single line from the data file
    int tmpYear, tmpMonth, tmpDay, tmpHour, tmpMinute;
    float tmpSecond, tmpVoltage, tmpCurrent, tmpFrequency;

    DataEntry previousEntry;
    DataEntry currentEntry;

    int counter = 0; // Used to keep track of the number of entries in the data file

    ifstream infile("data.txt");

    // Get the values of the first line of the data file so we have something to compare against in the first loop of our program
    // We loop through the data until we find a valid entry to start with
    do
    {
        infile >> tmpYear >> tmpMonth >> tmpDay >> tmpHour >> tmpMinute >> tmpSecond >> tmpVoltage >> tmpCurrent >> tmpFrequency;
        previousEntry.setValues(tmpYear, tmpMonth, tmpDay, tmpHour, tmpMinute, tmpSecond, tmpVoltage, tmpCurrent, tmpFrequency);
    }
    while (!previousEntry.isValid() && !infile.eof()); // If every entry is invalid, we need to know when we've reached the end of the file to break from this loop


    MaxPower = previousEntry.getPowerRMS();
    MeanPower += previousEntry.getPowerRMS();
    // We divide the power by 1000 to get it in kW and we divide the time by 3600 to get it in hours i.e kWh
    counter++;

    while (!infile.eof())
    {
        infile >> tmpYear >> tmpMonth >> tmpDay >> tmpHour >> tmpMinute >> tmpSecond >> tmpVoltage >> tmpCurrent >> tmpFrequency;
        currentEntry.setValues(tmpYear, tmpMonth, tmpDay, tmpHour, tmpMinute, tmpSecond, tmpVoltage, tmpCurrent, tmpFrequency);

        if (currentEntry.isValid()) // Ensure that we only compare valid values
        {
            TotalEnergy += 0.5 * (previousEntry.getPowerRMS() + currentEntry.getPowerRMS())/1000 * GetTimeDifference(currentEntry, previousEntry)/3600;
            EnergyPerHourVector.at(currentEntry.getHour()) += 0.5 * (previousEntry.getPowerRMS() + currentEntry.getPowerRMS())/1000 * GetTimeDifference(currentEntry, previousEntry)/3600;
            MeanPower += currentEntry.getPowerRMS();

            if (currentEntry.getPowerRMS() > MaxPower)
                MaxPower = currentEntry.getPowerRMS();

            counter++;

            previousEntry = currentEntry;
        }
    }

    infile.close();

    MeanPower /= counter;
}

// We pass the vector argument by reference to avoid making a copy of it in memory. We make it constant to avoid modifying any of the values within the vector.
void WriteToFiles(float TotalEnergy, float MaxPower, float MeanPower, const vector<float>& EnergyPerHourVector)
{
    ofstream outfile("stats.txt");

    outfile << TotalEnergy << endl;
    outfile << MeanPower << endl;
    outfile << MaxPower << endl;

    outfile.close();

    outfile.open("byhour.txt");

    for (int i = 0; i <= 23; i++)
        outfile << i << " " << EnergyPerHourVector.at(i) << endl;

    outfile.close();
}

float GetTimeDifference(DataEntry CurrentEntry, DataEntry PreviousEntry)
{
    // We take the absolute value in case our time differences are negative i.e The time is reversed for two entries
    return abs(( CurrentEntry.getHour() * 3600 + CurrentEntry.getMinute() * 60 + CurrentEntry.getSecond() ) - ( PreviousEntry.getHour() * 3600 + PreviousEntry.getMinute() * 60 + PreviousEntry.getSecond()));
}
